package com.allstate.JavaProject.rest;

import com.allstate.JavaProject.entities.Payment;
import com.allstate.JavaProject.services.PaymentBs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Collection;
import java.util.List;

@CrossOrigin(value = "*")
@RestController
@RequestMapping("/api")
public class PaymentController {
    @Autowired
    private PaymentBs paymentService;

    @RequestMapping(value = "/rowcount", method = RequestMethod.GET)
    public int rowcount() {
        return paymentService.rowcount();
    }

    @RequestMapping(value="/findid/{id}", method=RequestMethod.GET)
    public Payment findById(@PathVariable("id")int id){
        return paymentService.findById(id);
    }

    @RequestMapping(value="/findpaymenttype/{type}", method=RequestMethod.GET)
    public Collection<Payment> findByType(@PathVariable("type")String type){
        return paymentService.findByType(type);
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public void save (@RequestBody Payment payment) {
        paymentService.save(payment);
    }

    @RequestMapping(value = "/status", method = RequestMethod.GET)
    public String status()
    {
        return "Rest Api is running";
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<Payment> findAll(){
        return paymentService.findAll();
    }
}
