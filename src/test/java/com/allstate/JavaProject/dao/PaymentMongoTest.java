package com.allstate.JavaProject.dao;


import com.allstate.JavaProject.entities.Payment;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;


import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class PaymentMongoTest {
    @Autowired PaymentDao dao;


    @Test
    public void testSave() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();

        Payment payment1 = new Payment(1, date, "Mortgage", 500.00, 1);
        Payment payment2 = new Payment(2, date, "Car", 200.00, 2);
        Payment payment3 = new Payment(3, date, "Rates", 50.00, 3);
        dao.save(payment1);
        dao.save(payment2);
        dao.save(payment3);

        Payment testPayment = dao.findById(1);
        assertNotNull(testPayment);
    }

    @Test
    public void testSave1() {
        boolean isSuccess = true;
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();

        Payment payment1 = new Payment(1, date, "Mortgage", 500.00, 1);
        isSuccess = dao.save(payment1);
        assertFalse(isSuccess);
    }

    @Test
    public void testRowCount() {
        int rowNumber = dao.rowcount();
        int testRowNumber = 0;
        assertNotEquals(testRowNumber,rowNumber);
    }

//    @Test
//    public void testRowCount1() {
//        int rowNumber = dao.rowcount();
//        int testRowNumber = 3;
//        assertEquals(testRowNumber,rowNumber);
//    }

    @Test
    public void testFindById() {
        Payment testPayment = dao.findById(1);
        assertNotNull(testPayment);
    }

    @Test
    public void testCollection() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        Payment payment4 = new Payment(4, date, "Mortgage", 600.00, 4);
        dao.save(payment4);
        String PaymentType = "Mortgage";
        Collection<Payment> paymentListByType = dao.findByType(PaymentType);

        assertEquals(2,paymentListByType.size());
    }

}
