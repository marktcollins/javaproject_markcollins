package com.allstate.JavaProject.dao;

import com.allstate.JavaProject.entities.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public class PaymentMongoImpl implements PaymentDao{
    @Autowired
   private MongoTemplate tpl;

    @Override
    public int rowcount() {
        Query query = new Query();
        Long rowCount = tpl.count(query, Payment.class);
        return rowCount.intValue();
    }
    @Override
    public Payment findById(int id){
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        Payment payment = tpl.findOne(query, Payment.class);
        return payment;
    }

    @Override
    public Collection<Payment> findByType(String type){
        Query query = new Query();
        query.addCriteria(Criteria.where("type").is(type));
        List<Payment> paymentList = tpl.find(query, Payment.class);
        return paymentList;
    }

    @Override
    public boolean save(Payment payment) {
        boolean isSuccess = false;
        Payment checkDuplicatePayment = findById(payment.getId());
        if (checkDuplicatePayment == null) {
            //tpl.insert(payment);
            tpl.save(payment);
            isSuccess = true;
        } else {
            isSuccess = false;
        }
        return isSuccess;
    }

    @Override
    public List<Payment> findAll(){
        return tpl.findAll(Payment.class);
    }


}
