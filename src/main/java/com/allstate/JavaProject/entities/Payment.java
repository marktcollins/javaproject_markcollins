package com.allstate.JavaProject.entities;

import com.allstate.JavaProject.exceptions.PaymentException;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
@Document
public class Payment {
    int id;
    Date paymentdate;
    String type;
    double amount;
    int custid;
    public Payment(){

    }
    public Payment(int id, Date paymentdate, String type, double amount, int custid) {
        this.id = id;
        this.paymentdate = paymentdate;
        this.type = type;
        this.amount = amount;
        this.custid = custid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getPaymentdate() {
        return paymentdate;
    }

    public void setPaymentdate(Date paymentdate) {
        this.paymentdate = paymentdate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        if(amount<0 )
        {
            throw new PaymentException("Payment must be greater than 0");
        }
        else
        {
            this.amount = amount;
        }
    }

    public int getCustid() {
        return custid;
    }

    public void setCustid(int custid) {
        this.custid = custid;
    }

    @Override
    public String toString() {
        StringBuilder instances = new StringBuilder();
        instances.append (getId());
        instances.append (",");
        instances.append (paymentdate);
        instances.append (",");
        instances.append (type);
        instances.append (",");
        instances.append (amount);
        instances.append (",");
        instances.append (custid);
        instances.append (",");
        return (instances.toString());
    }
}
