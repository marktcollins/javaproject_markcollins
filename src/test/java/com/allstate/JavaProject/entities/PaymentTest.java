package com.allstate.JavaProject.entities;

import org.junit.jupiter.api.Test;


import java.text.SimpleDateFormat;
import java.util.Date;



import static org.springframework.test.util.AssertionErrors.assertEquals;
import static org.springframework.test.util.AssertionErrors.assertNotEquals;

public class PaymentTest {

    @Test
    public void testGetAmount()
    {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        Payment payment = new Payment(1, date, "Mortgage", 500.00, 1);
        assertEquals("amount", 500.00, payment.getAmount());
    }

    @Test
    public void testGetId()
    {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        Payment payment = new Payment(1, date, "Mortgage", 500.00, 1);

        assertNotEquals("id", 3, payment.getId());
    }

    @Test
    public void testGetPayment()
    {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        Payment payment = new Payment(1, date, "Mortgage", 500.00, 1);
        assertEquals("paymentDate", date, payment.getPaymentdate());
    }

    @Test
    public void testGetType()
    {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        Payment payment = new Payment(1, date, "Mortgage", 500.00, 1);
        assertEquals("type", "Mortgage", payment.getType());
    }

    @Test
    public void testGetAmount2()
    {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        Payment payment = new Payment(1, date, "Mortgage", 500.00, 1);
        assertNotEquals("amount", 600.00, payment.getAmount());
    }

    @Test
    public void testGetCustid()
    {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        Payment payment = new Payment(1, date, "Mortgage", 500.00, 1);
        assertEquals("custid", 1, payment.getCustid());
    }

    @Test
    public void testStringBuilder()
    {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        Payment payment = new Payment(1, date, "Mortgage", 500.00, 1);
        //assertNotEquals("id", 3, payment.getId());
        String StringBuild = payment.toString();
        String[] parts = StringBuild.split(",");
        assertEquals("parts", "Mortgage", parts[2]);
    }
}
