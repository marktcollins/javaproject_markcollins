package com.allstate.JavaProject.dao;

import com.allstate.JavaProject.entities.Payment;

import java.util.Collection;
import java.util.List;

public interface PaymentDao {
    int rowcount();
    Payment findById(int id);
    Collection <Payment> findByType(String type);
    boolean save(Payment payment);
    List<Payment> findAll();
}
