package com.allstate.JavaProject.rest;

import com.allstate.JavaProject.dao.PaymentDao;
import com.allstate.JavaProject.services.PaymentBs;
import org.apache.coyote.Response;
import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import static org.junit.jupiter.api.Assertions.*;

import java.net.URI;
import java.net.URISyntaxException;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)

public class PaymentControllerTest {

    @LocalServerPort
    int port;

    @Test
    public void testStatus() throws URISyntaxException{
        TestRestTemplate restTemplate = new TestRestTemplate();
        final String baseurl = "http://localhost:" + port + "/api/status";
        URI uri = new URI(baseurl);
        ResponseEntity<String> result = restTemplate.getForEntity(uri, String.class);
        assertEquals(200, result.getStatusCodeValue());
        assertTrue(result.getBody().contains("Rest Api is running"));
    }

}
