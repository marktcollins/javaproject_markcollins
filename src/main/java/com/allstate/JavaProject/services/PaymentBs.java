package com.allstate.JavaProject.services;

import com.allstate.JavaProject.dao.PaymentDao;
import com.allstate.JavaProject.entities.Payment;
import com.allstate.JavaProject.exceptions.PaymentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.Collection;
import java.util.List;

//@CrossOrigin(value = "*")
@Service
public class PaymentBs {
    @Autowired
    private PaymentDao dao;


    public int rowcount() {
        int rowCount = dao.rowcount();
        return rowCount;
    }

    public Payment findById(int id){
        Payment payment = new Payment();
        if (id > 0) {
            payment = dao.findById(id);
        } else {
            throw new PaymentException("Id must be greater than 0");
        }
        return payment;
    }


    public Collection<Payment> findByType(String type){
        Collection<Payment> paymentList = null;
        if (type == null) {
            throw new PaymentException("Type must not be null");
        } else {
            paymentList = dao.findByType(type);
        }
        return paymentList;
    }


    public boolean save(Payment payment) {
        boolean isSuccess = dao.save(payment);
        return isSuccess;
    }


    public List<Payment> findAll(){
        return dao.findAll();
    }

}
