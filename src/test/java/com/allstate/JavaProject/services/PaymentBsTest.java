package com.allstate.JavaProject.services;
import com.allstate.JavaProject.dao.PaymentDao;
import com.allstate.JavaProject.dao.PaymentMongoImpl;
import com.allstate.JavaProject.entities.Payment;
import com.allstate.JavaProject.exceptions.PaymentException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class PaymentBsTest {
    @Autowired
    PaymentDao dao;


    @Autowired
    PaymentBs bs;

    @Test
    public void testSave() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        Payment payment1 = new Payment(1, date, "Mortgage", 500.00, 1);
        Payment payment2 = new Payment(2, date, "Car", 200.00, 2);
        Payment payment3 = new Payment(3, date, "Rates", 50.00, 3);
        bs.save(payment1);
        bs.save(payment2);
        bs.save(payment3);

        PaymentMongoImpl paymentMongoImpl= new PaymentMongoImpl();
        Payment testPayment = bs.findById(1);
        assertNotNull(testPayment);
    }

    @Test
    public void testRowCount() {
        int rowNumber = bs.rowcount();
        int testRowNumber = 1;
        assertNotEquals(testRowNumber,rowNumber);
    }

//    @Test
//    public void testRowCount1() {
//        int rowNumber = bs.rowcount();
//        int testRowNumber = 3;
//        assertEquals(testRowNumber,rowNumber);
//    }

    @Test
    public void testExceptionFindById() {
        Exception exception = assertThrows(PaymentException.class, () -> {bs.findById(0);});
        String expectedMessage = "Id must be greater than 0";
        String actualMessage = exception.getMessage();
       assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void testFindById() {
        Payment testPayment = bs.findById(1);
        assertNotNull(testPayment);
    }

    @Test
    public void testFindByTypeException() {
        String PaymentType = null;
        Exception exception = assertThrows(PaymentException.class, () -> {bs.findByType(PaymentType);});
        String expectedMessage = "Type must not be null";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void testFindByType() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        Payment payment4 = new Payment(4, date, "Mortgage", 600.00, 4);
        bs.save(payment4);
        String PaymentType = "Mortgage";
        Collection<Payment> paymentListByType = bs.findByType(PaymentType);
        assertEquals(2,paymentListByType.size());
    }
}
